package hello;

import java.util.HashMap;
import java.util.Map;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;



@Controller
public class CVController {
    Map<String, String> cv = new HashMap<>();

    @GetMapping("/cv")
    public String cv(@RequestParam(name = "visitor", required = false)
                             String visitor, Model model) {
        model.addAttribute("visitor", visitor);
        attributes();
        model.addAllAttributes(cv);
        return "cv";
    }

    private void attributes() {
        cv.put("owner", "Alsabila Shakina Prasetyo");
        cv.put("birthDate", "7 Maret 1998");
        cv.put("birthPlace", "Jakarta");
        cv.put("address", "Perumahan Raffles Hills Blok E11 No 19");
        cv.put("sd", "SD Al-Jannah");
        cv.put("smp", "SMP N 49 Jakarta");
        cv.put("sma", "SMA N 48 Jakarta");
        cv.put("univ", "Universitas Indonesia");
        String description = "Nice to meet you";
        cv.put("description", description);
    }
}
