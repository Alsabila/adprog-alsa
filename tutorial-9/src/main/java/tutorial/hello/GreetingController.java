package tutorial.hello;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.concurrent.atomic.AtomicLong;

public class GreetingController {

    private static final String template = "Hello, %s! Welcome to JAVAri Park!";
    private final AtomicLong counter = new AtomicLong();

    public tutorial.hello.Greeting greeting(@RequestParam(value = "name",
            defaultValue = "World") String name) {
        return new tutorial.hello.Greeting(counter.incrementAndGet(),
                            String.format(template, name));
    }
}
