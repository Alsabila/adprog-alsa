package tutorial.javari;

import org.springframework.web.bind.annotation.*;
import tutorial.javari.animal.Animal;

import java.util.List;
import java.util.concurrent.atomic.AtomicLong;

@RestController
public class JavariController {

    private final AtomicLong counter = new AtomicLong();

    private List<Animal> animals;

    @RequestMapping(value = "/javari", method = RequestMethod.GET)
    public List<Animal> getListOfAnimals() {
        if (animals == null) {
            setUpListAnimals();
        }

        return JavariCsvMapper.getListAnimals();
    }

    @RequestMapping(value = "/javari/{id}", method = RequestMethod.GET)
    public Animal getAnimalById(@PathVariable("id") int id) {
        if (animals == null) {
            setUpListAnimals();
        }

        return JavariCsvMapper.getAnimal(id);
    }

    private void setUpListAnimals() {
        animals = JavariCsvMapper.getListAnimals();
        counter.set(animals.get(animals.size() - 1).getId());
    }
}