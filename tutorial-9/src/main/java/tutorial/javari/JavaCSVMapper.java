package java.tutorial.javari;
import tutorial.javari.animal.Animal;
import tutorial.javari.animal.Condition;
import tutorial.javari.animal.Gender;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;


public class JavaCSVMapper {
    private static final String filePath = "tutorial-9/src/main/java/tutorial/javari/animals.csv";
    private static final Path csv = Paths.get("", filePath);

    public static List<Animal> getListAnimals() {
        List<Animal> animals = new ArrayList<>();

        try {
            Files.lines(csv)
                    .forEach(i -> animals.add(createAnimalFromCsv(i)));
        } catch (IOException e) {
            e.printStackTrace();
        }

        return animals;
    }

    public static Animal getAnimal(int id) {
        List<Animal> animals = new ArrayList<>();

        try {
            Files.lines(csv)
                    .filter(i -> Integer.parseInt(i.split(",")[0]) == id)
                    .forEach(match -> animals.add(createAnimalFromCsv(match)));
        } catch (IOException e) {
            e.printStackTrace();
        }

        if (animals.size() == 1) {
            return animals.get(0);
        } else {
            return null;
        }
    }

    public static Animal createAnimalFromCsv(String csvLine) {
        String[] splitCsv = csvLine.split(",");
        int id = Integer.parseInt(splitCsv[0]);
        String type = splitCsv[1].trim();
        String name = splitCsv[2].trim();
        Gender gender = Gender.parseGender(splitCsv[3].trim());
        double length = Double.parseDouble(splitCsv[4]);
        double weight = Double.parseDouble(splitCsv[5]);
        Condition condition = Condition.parseCondition(splitCsv[6].trim());

        return new Animal(id,type,name, new Gender(gender), length, weight, new Condition(condition));
    }

}
