package id.ac.ui.cs.advprog.tutorial4.exercise1.factory.sauce;

public class ThousandIslandSauce implements Sauce {
    public String toString() {
        return "Thousand Island Sauce";
    }
}
