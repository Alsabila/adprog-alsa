package id.ac.ui.cs.advprog.tutorial4.exercise1.factory.clam;

public class MagicClams implements Clams {

    public String toString() {
        return "Magic Clams from Chesapeake Bay";
    }
}
