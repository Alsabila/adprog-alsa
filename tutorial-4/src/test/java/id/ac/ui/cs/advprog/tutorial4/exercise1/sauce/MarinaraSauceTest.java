package id.ac.ui.cs.advprog.tutorial4.exercise1.sauce;

import static org.junit.Assert.assertEquals;

import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.sauce.MarinaraSauce;
import org.junit.Before;
import org.junit.Test;

public class MarinaraSauceTest {
    private MarinaraSauce mar;

    @Before
    public void setUp() {
        mar = new MarinaraSauce();
    }

    @Test
    public void tesToString() {
        assertEquals("Marinara Sauce", mar.toString());
    }
}
