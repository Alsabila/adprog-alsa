package id.ac.ui.cs.advprog.tutorial4.exercise1.clam;

import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.clam.MagicClams;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class MagicClamsTest {
    private MagicClams magic;

    @Before
    public void setUp() {
        magic = new MagicClams();
    }

    @Test
    public void tesToString() {
        assertEquals("Magic Clams from Chesapeake Bay", magic.toString());
    }
}
