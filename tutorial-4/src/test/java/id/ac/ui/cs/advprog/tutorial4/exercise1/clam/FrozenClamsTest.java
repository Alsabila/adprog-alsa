package id.ac.ui.cs.advprog.tutorial4.exercise1.clam;

import static org.junit.Assert.assertEquals;

import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.clam.FrozenClams;
import org.junit.Before;
import org.junit.Test;

public class FrozenClamsTest {
    private FrozenClams fro;

    @Before
    public void setUp() {
        fro = new FrozenClams();
    }

    @Test
    public void tesToString() {
        assertEquals("Frozen Clams from Chesapeake Bay", fro.toString());
    }
}
