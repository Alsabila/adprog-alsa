package id.ac.ui.cs.advprog.tutorial4.exercise1.dough;

import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.dough.StuffedCrustDough;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.dough.ThickCrustDough;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class StuffedCrustDoughTest {
    private StuffedCrustDough stuffed;

    @Before
    public void setUp() {
        stuffed = new StuffedCrustDough();
    }

    @Test
    public void tesToString() {
        assertEquals("Stuffed Crust Dough", stuffed.toString());
    }
}
