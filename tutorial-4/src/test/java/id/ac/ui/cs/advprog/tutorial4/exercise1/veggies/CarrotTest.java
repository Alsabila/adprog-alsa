package id.ac.ui.cs.advprog.tutorial4.exercise1.veggies;

import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.veggies.Carrot;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class CarrotTest {
    private Carrot carrot;

    @Before
    public void setUp() {
        carrot= new Carrot();
    }

    @Test
    public void tesToString() {
        assertEquals("Carrot", carrot.toString());
    }
}
