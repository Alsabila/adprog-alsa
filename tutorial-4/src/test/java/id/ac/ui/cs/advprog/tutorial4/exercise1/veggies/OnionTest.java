package id.ac.ui.cs.advprog.tutorial4.exercise1.veggies;

import static org.junit.Assert.assertEquals;

import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.veggies.Onion;
import org.junit.Before;
import org.junit.Test;

public class OnionTest {
    private Onion oni;

    @Before
    public void setUp() {
        oni = new Onion();
    }

    @Test
    public void tesToString() {
        assertEquals("Onion", oni.toString());
    }
}
