package id.ac.ui.cs.advprog.tutorial4.exercise1.sauce;

import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.sauce.ThousandIslandSauce;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class ThousandIslandSauceTest {
    private ThousandIslandSauce island;

    @Before
    public void setUp() {
        island = new ThousandIslandSauce();
    }

    @Test
    public void tesToString() {
        assertEquals("Thousand Island Sauce", island.toString());
    }
}
