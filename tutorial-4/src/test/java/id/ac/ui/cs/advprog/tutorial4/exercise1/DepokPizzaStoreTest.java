package id.ac.ui.cs.advprog.tutorial4.exercise1;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import id.ac.ui.cs.advprog.tutorial4.exercise1.pizza.Pizza;
import org.junit.Before;
import org.junit.Test;

public class DepokPizzaStoreTest {
    PizzaStore depok;
    Pizza cheesePizza;
    Pizza clamPizza;
    Pizza veggiesPizza;

    @Before
    public void setUp() {
        depok = new DepokPizzaStore();
        cheesePizza = depok.orderPizza("cheese");
        clamPizza = depok.orderPizza("clam");
        veggiesPizza = depok.orderPizza("veggie");
    }

    @Test
    public void checkPizzaNames() {
        assertEquals("Depok Style Cheese Pizza", cheesePizza.getName());
        assertEquals("Depok Style Clam Pizza", clamPizza.getName());
        assertEquals("Depok Style Veggie Pizza", veggiesPizza.getName());
    }

    @Test
    public void checkPizzaDescriptions() {
        assertNotNull(clamPizza.toString());
        assertNotNull(veggiesPizza.toString());
        assertNotNull(cheesePizza.toString());
    }
}
