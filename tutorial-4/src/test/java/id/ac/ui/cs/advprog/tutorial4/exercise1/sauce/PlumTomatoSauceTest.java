package id.ac.ui.cs.advprog.tutorial4.exercise1.sauce;

import static org.junit.Assert.assertEquals;

import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.sauce.PlumTomatoSauce;
import org.junit.Before;
import org.junit.Test;

public class PlumTomatoSauceTest {
    private PlumTomatoSauce plum;

    @Before
    public void setUp() {
        plum = new PlumTomatoSauce();
    }

    @Test
    public void tesToString() {
        assertEquals("Tomato sauce with plum tomatoes", plum.toString());
    }
}
