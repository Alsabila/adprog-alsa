package id.ac.ui.cs.advprog.tutorial4.exercise1.veggies;

import static org.junit.Assert.assertEquals;

import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.veggies.BlackOlives;
import org.junit.Before;
import org.junit.Test;

public class BlackOlivesTest {
    private BlackOlives blacky;

    @Before
    public void setUp() {
        blacky = new BlackOlives();
    }

    @Test
    public void tesToString() {
        assertEquals("Black Olives", blacky.toString());
    }
}
