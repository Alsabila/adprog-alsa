package id.ac.ui.cs.advprog.tutorial4.exercise1.cheese;

import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.cheese.MozzarellaCheese;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class MozzarellaCheeseTest {

    private MozzarellaCheese mozza;

    @Before
    public void setUp() {
        mozza = new MozzarellaCheese();
    }

    @Test
    public void tesToString() {
        assertEquals("Shredded Mozzarella", mozza.toString());
    }
}
