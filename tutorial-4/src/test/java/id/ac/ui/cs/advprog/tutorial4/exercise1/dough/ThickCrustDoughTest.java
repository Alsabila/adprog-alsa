package id.ac.ui.cs.advprog.tutorial4.exercise1.dough;

import static org.junit.Assert.assertEquals;

import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.dough.ThickCrustDough;
import org.junit.Before;
import org.junit.Test;

public class ThickCrustDoughTest {
    private ThickCrustDough thick;

    @Before
    public void setUp() {
        thick = new ThickCrustDough();
    }

    @Test
    public void tesToString() {
        assertEquals("Thick Crust Dough", thick.toString());
    }
}
