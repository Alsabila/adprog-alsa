package applicant;

import java.util.function.Predicate;

public class EvaluatorChain implements Evaluator {
    private Evaluator next;

    public EvaluatorChain(Evaluator nextEvaluator) {
        next = nextEvaluator;
    }

    public Predicate<Applicant> evaluate(Applicant applicant) {
        return next.evaluate(applicant);
    }
}
