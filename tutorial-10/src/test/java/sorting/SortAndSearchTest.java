package sorting;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.assertFalse;

import static sorting.Sorter.newSort;
import static sorting.Sorter.slowSort;

import static sorting.Finder.newSearch;
import static sorting.Finder.slowSearch;


import org.junit.Test;

public class SortAndSearchTest {
    //TODO Implement, apply your test cases here
    public boolean isArraySorted(int[] arr) {
        for (int i = 1; i < arr.length; ++i) {
            if (arr[i] < arr[i - 1]) {
                return false;
            }
        }
        return true;
    }

    @Test
    public void slowSearchReturnTrue() {
        int arr[] = {2, 1, 10, 11, 4, 8};
        assertEquals(slowSearch(arr, 10), 10);
    }

    @Test
    public void newSearchReturnTrue() {
        int arr[] = {2, 3, 5, 8, 9, 13, 16};
        assertEquals(newSearch(arr, 3),3);
    }

    @Test
    public void isSlowSortReturnSorted() {
        int arr[] = {1, -1, 2, -10, 10000, 8, 6, 3};
        assertTrue(isArraySorted(slowSort(arr)));
    }

    @Test
    public void isNewSortReturnSorted() {
        int arr[] = {-2, 4, -7, 99999, 49, 20, -3, 4};
        assertTrue(isArraySorted(newSort(arr)));
    }
}
