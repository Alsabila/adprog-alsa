package id.ac.ui.cs.advprog.tutorial3.decorator.filling;

import id.ac.ui.cs.advprog.tutorial3.decorator.Food;

public class BeefMeat extends Food {
    Food food;

    public BeefMeat(Food food) {
        //DONE Implement
    	this.food = food;
    }

    @Override
    public String getDescription() {
        //DONE Implement
    	return food.getDescription() + ", Beef Meat";
    }

    @Override
    public double cost() {
        //DONE Implement
    	return food.cost() + 6.0;
    }
}
