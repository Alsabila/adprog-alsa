package id.ac.ui.cs.advprog.tutorial3.decorator.filling;

import id.ac.ui.cs.advprog.tutorial3.decorator.Food;

public class Cheese extends Food {
    Food food;

    public Cheese(Food food) {
        //DONE Implement
    	this.food = food;
    }

    @Override
    public String getDescription() {
        //DONE Implement
    	return food.getDescription() + ", Cheese";
    }

    @Override
    public double cost() {
        //DONE Implement
    	return food.cost() + 2.0;
    }
}
