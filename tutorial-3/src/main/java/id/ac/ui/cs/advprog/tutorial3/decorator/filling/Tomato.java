package id.ac.ui.cs.advprog.tutorial3.decorator.filling;

import id.ac.ui.cs.advprog.tutorial3.decorator.Food;

public class Tomato extends Food {
    Food food;

    public Tomato(Food food) {
        //DONE Implement
    	this.food = food;
    }

    @Override
    public String getDescription() {
        //DONE Implement
    	return food.getDescription() + ", Tomato";
    }

    @Override
    public double cost() {
        //DONE Implement
    	return food.cost() + 0.5;
    }
}
